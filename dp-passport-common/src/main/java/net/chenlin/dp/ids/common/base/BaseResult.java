package net.chenlin.dp.ids.common.base;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

/**
 * 结果返回基类
 * @author zcl<yczclcn@163.com>
 */
public class BaseResult implements Serializable {

    private static final long serialVersionUID = -9041141273787233464L;

    /**
     * 相应码
     */
    private String respCode;

    /**
     * 错误信息
     */
    private String respMsg;

    /**
     * 返回数据
     */
    private Object respData;

    /**
     * constructor
     * @param respCode
     * @param respMsg
     * @param respData
     */
    public BaseResult(String respCode, String respMsg, Object respData) {
        this.respCode = respCode;
        this.respMsg = respMsg;
        this.respData = respData;
    }

    /**
     * constructor
     * @param respCode
     * @param respMsg
     */
    public BaseResult(String respCode, String respMsg) {
        this.respCode = respCode;
        this.respMsg = respMsg;
    }

    /**
     * constructor
     */
    public BaseResult() { }

    /**
     * getter for respCode
     * @return
     */
    public String getRespCode() {
        return respCode;
    }

    /**
     * setter for respCode
     * @return
     */
    public BaseResult setRespCode(String respCode) {
        this.respCode = respCode;
        return this;
    }

    /**
     * getter for respMsg
     * @return
     */
    public String getRespMsg() {
        return respMsg;
    }

    /**
     * setter for respMsg
     * @return
     */
    public BaseResult setRespMsg(String respMsg) {
        this.respMsg = respMsg;
        return this;
    }

    /**
     * getter for respData
     * @return
     */
    public Object getRespData() {
        return respData;
    }

    /**
     * setter for respData
     * @return
     */
    public BaseResult setRespData(Object respData) {
        this.respData = respData;
        return this;
    }

    /**
     * 是否成功
     * @return
     */
    @JSONField(serialize = false)
    public boolean success() {
        return "0000".equals(respCode);
    }

    /**
     * 是否系统异常
     * @return
     */
    @JSONField(serialize = false)
    public boolean sysError() {
        return "9999".equals(respCode);
    }

    /**
     * 是否业务异常
     * @return
     */
    @JSONField(serialize = false)
    public boolean bizError() {
        return "1000".equals(respCode);
    }

    /**
     * toString
     * @return
     */
    @Override
    public String toString() {
        return "BaseResult{" +
                "respCode='" + respCode + '\'' +
                ", respMsg='" + respMsg + '\'' +
                ", respData=" + respData +
                '}';
    }

}
